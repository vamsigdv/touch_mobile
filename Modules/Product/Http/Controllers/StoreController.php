<?php

namespace Modules\Product\Http\Controllers;

use App\Traits\ImageStore;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Http\Requests\CreateStoreRequest;
use Modules\Product\Http\Requests\UpdateStoreRequest;
use \Modules\Product\Services\StoreService;
use Modules\Product\Entities\Store;
use Modules\Product\Entities\StoreImage;
use Modules\Product\Entities\Brand;
use Illuminate\Support\Facades\DB;
use Exception;
use Modules\UserActivityLog\Traits\LogActivity;
use Yajra\DataTables\Facades\DataTables;

class StoreController extends Controller
{
    use ImageStore;
    protected $storeService;

    public function __construct(StoreService $storeService)
    {
        $this->middleware('maintenance_mode');
        $this->middleware('prohibited_demo_mode')->only('store');
        $this->storeService = $storeService;
    }

    public function index()
    {
    
        //$data['StoreList'] = $this->storeService->getAll();
       // print_r($data['StoreList']->storeImage);exit;
        try {
            $data['StoreList'] = $this->storeService->getAll();
            return view('product::store.index', $data);
        } catch (Exception $e) {
            LogActivity::errorLog($e->getMessage());
            Toastr::error(__('common.operation_failed'));
            return back();
        }
    }

    public function getData(){
        $store = $this->storeService->getData();
        return DataTables::of($store)
            ->addIndexColumn()
            ->editColumn('name', function ($store) {
                return  $store->name;
            })
            ->addColumn('status', function ($store) {
                return view('product::store.components._status_td', compact('store'));
            })
            ->addColumn('action', function ($store) {
                return view('product::store.components._action_td', compact('store'));
            })
            ->rawColumns(['status', 'action'])
            ->toJson();
    }

    public function info()
    {
        try {
            $data['StoreList'] = $this->storeService->getAll();
            return view('product::store.index_info', $data);
        } catch (Exception $e) {
            LogActivity::errorLog($e->getMessage());
            Toastr::error(__('common.operation_failed'));
            return back();
        }
    }

    public function bulk_category_upload_page()
    {
        return view('product::category.bulk_upload');
    }

    public function csv_category_download()
    {
        try {
            $this->storeService->csvDownloadCategory();
            $filePath = storage_path("app/category_list.xlsx");
        	$headers = ['Content-Type: text/xlsx'];
        	$fileName = time().'-category_list.xlsx';
            return response()->download($filePath, $fileName, $headers);
            return back();
        } catch (\Exception $e) {
            LogActivity::errorLog($e->getMessage());
            Toastr::error(__('common.Something Went Wrong'));
            return back();
        }
    }

    public function bulk_category_store (Request $request)
    {
        $request->validate([
            'file' => ['required','mimes:xls,xlsx,csv,txt','max:2048']
        ]);
        ini_set('max_execution_time', 0);
        DB::beginTransaction();
        try {
            $this->storeService->csvUploadCategory($request->except("_token"));
            DB::commit();
            LogActivity::successLog('Bulk category store successful.');
            Toastr::success(__('common.uploaded_successfully'),__('common.success'));
            return back();
        } catch (\Exception $e) {
            DB::rollBack();
            if ($e->getCode() == 23000) {
                Toastr::error(__('common.duplicate_entry_is_exist_in_your_file'));
            }
            else {
                Toastr::error(__('common.Something Went Wrong'));
            }
            LogActivity::errorLog($e->getMessage());
            return back();
        }
    }

    public function list()
    {
        $StoreList = $this->storeService->getAll();
        return view('product::store.components.list', compact('StoreList'));
    }

    public function create()
    {
        try {
            $data['StoreList']=Store::with(['storeImage'])->get();
            $data['BrandList']=Brand::get();
            return response()->json([
                'editHtml' => (string)view('product::store.components.create',$data)
            ]);
        } catch (Exception $e) {
            LogActivity::errorLog($e->getMessage());
            return $e->getMessage();
        }
    }

    public function store(CreateStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $this->storeService->save($request->except('_token'));
            DB::commit();
            LogActivity::successLog('Store Added.');
            if(isset($request->form_type)){
                if($request->form_type == 'modal_form'){
                    $first_store = $this->storeService->firstStore();
                    return response()->json([
                        'storeSelect' =>  (string)view('product::products.components._store_list_select', compact('first_store')),
                        'storeParentList' =>  (string)view('product::products.components._store_parent_list', compact('first_store'))
                    ]);
                }else{
                    return  $this->loadTableData();
                }
            }else{
                return  $this->loadTableData();
            }
        } catch (Exception $e) {
            DB::rollBack();
            LogActivity::errorLog($e->getMessage());
            return response()->json([
                'status'    =>  false,
                'message'   =>  $e
            ],503);
        }
    }

    public function edit($id)
    {
        try {
            $StoreList = $this->storeService->getAll();
            $store = $this->storeService->editById($id);
            //print_r($store->storeImage->store_image_media);exit;
            return response()->json([
               'editHtml' => (string)view('product::store.components.edit',compact('StoreList','store')),
               'status' => true
            ]);
        } catch (Exception $e) {
            LogActivity::errorLog($e->getMessage());
            return response()->json([
                'status'    =>  false,
                'message'   =>  $e
            ]);
        }
    }

    public function update(UpdateStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $this->storeService->update($request->except('_token'), $request->id);
            DB::commit();
            LogActivity::successLog('Store Updated.');
            return  $this->loadTableData();
        } catch (Exception $e) {
            DB::rollBack();
            LogActivity::errorLog($e->getMessage());
            return response()->json([
                'status'    =>  false,
                'message'   =>  $e
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            
            $result = $this->storeService->deleteById($request['id']);
            if ($result == "not_possible") {
                return response()->json([
                    'parent_msg' => __('common.related_data_exist_in_multiple_directory')
                ]);
            }
            LogActivity::successLog('store delete successful.');
            return  $this->loadTableData();
        } catch (Exception $e) {
            LogActivity::errorLog($e->getMessage());
            return response()->json([
                'status'    =>  false,
                'message'   =>  $e
            ]);
        }
    }


    private function loadTableData()
    {
        try {
            $StoreList = $this->storeService->getAll();
            return response()->json([
                'TableData' =>  (string)view('product::store.components.list'),
                'createForm' =>  (string)view('product::store.components.create', compact(['StoreList']))
            ]);
        } catch (\Exception $e) {
            LogActivity::errorLog($e->getMessage());
            Toastr::error(__('common.operation_failed'));
            return response()->json([
                'error' => $e->getMessage()
            ],503);
        }
    }

    public function newCategory(){
        return view('product::new_category.index');
    }

    public function newCategorySetup(){
        return view('product::new_category.components.setup');
    }
}
