<?php

namespace Modules\Product\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isModuleActive('FrontendMultiLang')) {
            $code = auth()->user()->lang_code;
            return [
                'name.'. $code => "required|unique_translation:stores,name,{$this->id}",
                'slug' => 'required|unique:stores,slug,'.$this->id,
                'status' => 'required',
                'searchable' => 'required',
                //'category_image' => 'nullable'
            ];
        }else{
            return [
                'name' => 'required|unique:stores,name,'.$this->id,
                'slug' => 'required|unique:stores,slug,'.$this->id,
                'status' => 'required',
                'searchable' => 'required',
                //'category_image' => 'nullable'
            ];
        }
    }

    public function messages()
    {
        if (isModuleActive('FrontendMultiLang')) {
            return [
                'name.*.required' => 'The Store name is required',
                'name.*.unique_translation' => 'The Store name has already been taken',
            ];
        }else{
            return [
                'name.required' => 'The Store name is required',
                'name.unique' => 'The Store name has already been taken',
            ];
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
