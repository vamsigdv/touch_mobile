<?php

namespace Modules\Product\Services;
use \Modules\Product\Repositories\StoreRepository;

class StoreService{

    protected $storeRepository;

    public function __construct(StoreRepository  $storeRepository)
    {
        $this->storeRepository= $storeRepository;
    }

    public function getModel()
    {
        return $this->storeRepository->getModel();
    }

    public function store()
    {
        return $this->storeRepository->store();
    }


    public function save($data)
    {
        //print_r($data);exit;
        return $this->storeRepository->save($data);
    }

    public function update($data,$id)
    {
        return $this->storeRepository->update($data, $id);
    }

    public function getAll()
    {
        return $this->storeRepository->getAll();
    }
    public function getData(){
        return $this->storeRepository->getData();
    }
    public function getActiveAll(){
        return $this->storeRepository->getActiveAll();
    }

    public function getCategoryByTop(){
        return $this->storeRepository->getCategoryByTop();
    }


    public function deleteById($id)
    {
        return $this->storeRepository->delete($id);
    }

    public function showById($id)
    {
        return $this->storeRepository->show($id);
    }

    public function editById($id){
        return $this->storeRepository->edit($id);
    }
   
    public function findBySlug($slug)
    {
        return $this->storeRepository->findBySlug($slug);
    }

    public function csvUploadCategory($data)
    {
        return $this->storeRepository->csvUploadCategory($data);
    }

    public function csvDownloadCategory()
    {
        return $this->storeRepository->csvDownloadCategory();
    }

    public function firstStore(){
        return $this->storeRepository->firstStore();
    }

}
