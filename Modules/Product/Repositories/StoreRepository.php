<?php
namespace Modules\Product\Repositories;
use App\Traits\ImageStore;
use Modules\Product\Entities\Store;
use Modules\Product\Entities\StoreImage;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Product\Imports\CategoryImport;
use Modules\Product\Export\CategoryExport;
use App\Models\MediaManager;
use App\Models\UsedMedia;

use function Clue\StreamFilter\fun;

class StoreRepository
{
    use ImageStore;
    protected $store;
    protected $ids = [];

    public function __construct(Store $store)
    {
        $this->store = $store;
    }
    public function store()
    {
        return Store::with(['storeImage'])->paginate(10);
    }
    /*public function store()
    {
        return Store::with(['brands', 'categoryImage', 'groups.categories','subCategories'])->where("parent_id", 0)->paginate(10);
    }
    public function activeStore()
    {
        return Store::with(['brands', 'categoryImage', 'groups.categories','subCategories'])->where("parent_id", 0)->where('status', 1)->paginate(20);
    }*/
    /*public function getData(){
        return Store::with(['subCategories','categoryImage'])->latest();
    }*/
    public function getData(){
        return Store::with(['storeImage'])->latest();
    }
    public function getModel(){
        return $this->store;
    }
    public function getAll()
    {
        //echo 'hello';exit;
        if(isModuleActive('Affiliate')){
            return Store::with('storeImage')->take(100)->get();
        }else{
            return Store::with('storeImage')->take(100)->get();
        }

    }
    public function getActiveAll(){
        return Store::where('status', 1)->latest()->get();
    }
   /* public function getCategoryByTop(){

        return Category::with('categoryImage', 'parentCategory', 'subCategories')->where('status', 1)->orderBy('total_sale', 'desc')->get();
    }*/
    public function save($data)
    {
       
        $store = new Store();
        $store->fill($data)->save();
        $host = activeFileStorage();
        $thumbnail_image = null;
        if(isset($data['store_image'])){
            $media_img = MediaManager::find($data['store_image']);
            if($media_img){
                if($media_img->storage == 'local'){
                    $file = asset_path($media_img->file_name);
                }else{
                    $file = $media_img->file_name;
                }
                $thumbnail_image = ImageStore::saveImage($file, 225, 225);
            }
            if ($host == 'Dropbox') {
                $data['image'] = $thumbnail_image['images_source'];
                $data['file_dropbox'] = $thumbnail_image['file_dropbox'];
            }else{
                $data['image'] = $thumbnail_image;
            }
          $cat_image = StoreImage::create([
                'store_id' => $store->id,
                'image' => $data['image']
            ]);
            if (isset($data['store_image'])) {
                UsedMedia::create([
                    'media_id' => $data['store_image'],
                    'usable_id' => $cat_image->id,
                    'usable_type' => get_class($cat_image),
                    'used_for' => 'store_image'
                ]);
            }
        }
        return true;
    }
    public function update($data, $id)
    {
        $store = Store::find($id);
        $store->fill($data)->save();
        $host = activeFileStorage();
        if (isset($data['store_image']) && $data['store_image'] != @$store->store_image_media->media_id) {
            if(@$store->storeImage->image != null){
                ImageStore::deleteImage($store->storeImage->image);
            }
            $media_img = MediaManager::find($data['store_image']);
            if($media_img->storage == 'local'){
                $file = asset_path($media_img->file_name);
            }else{
                $file = $media_img->file_name;
            }
            $store_image = ImageStore::saveImage($file, 225, 225);
            if ($host == 'Dropbox') {
                $data['image'] = $store_image['images_source'];
                $data['file_dropbox'] = $store_image['file_dropbox'];
            }else{
                $data['image'] = $store_image;
            }
            if ($store->storeImage) {
                $prev_meta = UsedMedia::where('usable_id', $store->storeImage->id)->where('usable_type', get_class($store->storeImage))->where('used_for', 'store_image')->first();
            }else {
                $prev_meta = '';
            }
            if($prev_meta){
                $prev_meta->update([
                    'media_id' => $media_img->id
                ]);
            }else{
              $storeImage =  StoreImage::create([
                        'store_id' => $id,
                        'image' => $data['image']
                    ]);
                UsedMedia::create([
                    'media_id' => $media_img->id,
                    'usable_id' => $storeImage->id,
                    'usable_type' => get_class($storeImage),
                    'used_for' => 'store_image'
                ]);
                return true;
            }
        }else{
            if($store->storeImage->store_image_media != null && !isset($data['store_image'])){
                $store->storeImage->store_image_media->delete();
                ImageStore::deleteImage($store->storeImage->image);
                $data['image'] = null;
            }else{
                $data['image'] = $store->storeImage->image;
            }
        }
        if(!empty($data['store_image'])){
            if(@$store->storeImage->image){
                ImageStore::deleteImage(@$store->storeImage->image);
                @$store->storeImage->update([
                    'image' => $data['image']
                ]);
            }else{
                StoreImage::create([
                    'store_id' => $id,
                    'image' => $data['image']
                ]);
            }
         }
        return true;
    }
    public function delete($id)
    {
        $store = Store::find($id);;
       /* if ($category->storeImage->image) {
                ImageStore::deleteImage($category->storeImage->image);
            }
        UsedMedia::where('usable_id', $category->storeImage->id)->where('usable_type', get_class($category->StoreImage))->where('used_for', 'category_image')->delete();*/
        $store->delete();
        return 'possible';
        
    }
    public function checkParentId($id){
         Category::where('parent_id',$id)->get();
    }
    public function show($id)
    {
        $store = $this->category->with('storeImage')->where('id', $id)->first();
        return $store;
    }
    public function edit($id){
        $store = $this->store->findOrFail($id);
        return $store;
    }
    public function findBySlug($slug)
    {
        return $this->category->where('slug', $slug)->first();
    }
    public function csvUploadCategory($data)
    {

        Excel::import(new CategoryImport, $data['file']);
    }
    public function csvDownloadCategory()
    {
        if (file_exists(storage_path("app/category_list.xlsx"))) {
          unlink(storage_path("app/category_list.xlsx"));
        }
        return Excel::store(new CategoryExport, 'category_list.xlsx');
    }
    public function getCategoryBySearch($search,$depend){
        $items = collect();
        if($search != ''){
            if ($depend) {
                $items = Category::with('subCategories')->where('status', 1)->where('parent_id', '!=' , $depend)->where('name', 'LIKE', "%{$search}%")->paginate(10);
            }else {
                $items = Category::with('subCategories')->where('status', 1)->where('name', 'LIKE', "%{$search}%")->paginate(10);
            }
        }else{
            if ($depend) {
                $items =  Category::with(['subCategories'])->where('parent_id', 0)->where('parent_id', '!=' , $depend)->where('status', 1)->paginate(10);
            }else {
                $items = Category::with(['subCategories'])->where('parent_id', 0)->where('status', 1)->paginate(10);
            }
        }
        $response = [];
        foreach($items as $category){
            if($depend && $category->id == $depend){
                if($category->subCategories->count() > 1){
                    $subData = $this->recuseSub($category->subCategories, $response, $depend);
                    $response = $subData;
                }
            }else {
                $level = '';
                for($i = 1; $i <= $category->depth_level ; $i++){
                    $level .= '-';
                }
                $level .= '> ';
                $response[]  =[
                    'id'    =>$category->id,
                    'text'  => $level.$category->name
                ];
                if($category->subCategories->count() > 1){
                    $subData = $this->recuseSub($category->subCategories, $response, $depend);
                    $response = $subData;
                }
            }

        }
        return  $response;
    }
    private function recuseSub($subcategories, $response, $depend = null){
        foreach($subcategories as $subcat){
            if($depend && $subcat->id == $depend ){
                if($subcat->subCategories->count() > 1){
                    $subData = $this->recuseSub($subcat->subCategories, $response, $depend);
                    $response = $subData;
                }
            } else{
                $level = '';
                for($i = 1; $i <= $subcat->depth_level ; $i++){
                    $level .= '-';
                }
                $level .= '> ';
                $response[]  =[
                    'id'    =>$subcat->id,
                    'text'  =>$level.$subcat->name
                ];
                if($subcat->subCategories->count() > 1){
                    $subData = $this->recuseSub($subcat->subCategories, $response, $depend);
                    $response = $subData;
                }
            }

        }
        return $response;
    }
    public function firstStore(){
        $store = Store::where('status', 1)->first();
        if($store){
            return $store;
        }
        return null;
    }
    public function getParentCategoryBySearch($search){
        $items = collect();
        if($search != ''){
            $items = Category::where('status', 1)->where('parent_id', 0)->where('name', 'LIKE', "%{$search}%")->paginate(10);
        }else{
            $items = Category::where('parent_id', 0)->where('status', 1)->paginate(10);
        }
        $response = [];
        foreach($items as $category){
            $level = '';
            for($i = 1; $i <= $category->depth_level ; $i++){
                $level .= '-';
            }
            $level .= '> ';
            $response[]  =[
                'id'    =>$category->id,
                'text'  => $level.$category->name
            ];
        }
        return $response;
    }
}
