<div class="dropdown CRM_dropdown">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ __('common.select') }}
    </button>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
        <!--<a data-value="{{$store}}" class="dropdown-item show_store">{{ __('common.show') }}</a>-->
        <a data-id="{{$store->id}}" class="dropdown-item copy_id">{{ __('product.Copy ID') }}</a>
        @if (permissionCheck('product.store.edit'))
            <a class="dropdown-item edit_store" data-id="{{$store->id}}">{{__('common.edit')}}</a>
        @endif
        @if (permissionCheck('product.store.delete'))
            <a class="dropdown-item delete_brand" data-id="{{$store->id}}">{{__('common.delete')}}</a>
        @endif
    </div>
</div>