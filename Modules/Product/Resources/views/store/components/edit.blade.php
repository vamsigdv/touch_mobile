<div class="main-title">
    <h3 class="mb-30">
        {{ __('product.edit_store') }}
    </h3>
</div>
@if(isModuleActive('FrontendMultiLang'))
@php
$LanguageList = getLanguageList();
@endphp
@endif
<form method="POST" action="" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data"
id="store_edit_form">
<input type="hidden" id="item_id" name="id" value="{{$store->id}}" />
    <div class="white-box">
        <div class="add-visitor">
            <div class="row">
            @if(isModuleActive('FrontendMultiLang'))
                <div class="col-lg-12">
                    <ul class="nav nav-tabs justify-content-start mt-sm-md-20 mb-30 grid_gap_5" role="tablist">
                        @foreach ($LanguageList as $key => $language)
                            <li class="nav-item lang_code" data-id="{{$language->code}}">
                                <a class="nav-link anchore_color @if (auth()->user()->lang_code == $language->code) active @endif" href="#element{{$language->code}}" role="tab" data-toggle="tab" aria-selected="@if (auth()->user()->lang_code == $language->code) true @else false @endif">{{ $language->native }} </a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach ($LanguageList as $key => $language)
                            <div role="tabpanel" class="tab-pane fade @if (auth()->user()->lang_code == $language->code) show active @endif" id="element{{$language->code}}">
                                <div class="primary_input mb-25">
                                    <label class="primary_input_label" for="name">
                                        {{__('common.name')}}
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input class="primary_input_field name" type="text" id="name{{auth()->user()->lang_code == $language->code?$language->code:''}}" name="name[{{$language->code}}]" autocomplete="off"  placeholder="{{__('common.name')}}" value="{{isset($store)?$store->getTranslation('name',$language->code):old('name.'.$language->code)}}">
                                    <span class="text-danger" id="error_name_{{$language->code}}"></span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <div class="col-lg-12">
                    <div class="primary_input mb-25">
                        <label class="primary_input_label" for="name">
                            {{__('common.name')}}
                            <span class="text-danger">*</span>
                        </label>
                        <input class="primary_input_field name" type="text" id="name" name="name" autocomplete="off" value="{{$store->name}}" placeholder="{{__('common.name')}}">
                    </div>
                    <span class="text-danger" id="error_name"></span>
                </div>
            @endif
                <div class="col-lg-12">
                    <div class="primary_input mb-25">
                        <label class="primary_input_label" for="slug">
                           {{__('common.slug')}}
                            <span class="text-danger">*</span>
                        </label>
                        <input class="primary_input_field slug" type="text" id="slug" name="slug" autocomplete="off" value="{{$store->slug}}" placeholder="{{__('common.slug')}}">
                    </div>
                    <span class="text-danger"  id="error_slug"></span>
                </div>

               
                <div class="col-xl-12 mt-20">
                    <div class="primary_input">
                        <label class="primary_input_label" for="">{{ __('product.searchable') }}</label>
                        <ul id="theme_nav" class="permission_list sms_list ">
                            <li>
                                <label data-id="bg_option" class="primary_checkbox d-flex mr-12 extra_width">
                                    <input name="searchable" id="searchable_active" value="1" {{$store->searchable == 1?'checked':''}}
                                        class="active" type="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <p>{{ __('common.active') }}</p>
                            </li>
                            <li>
                                <label data-id="color_option" class="primary_checkbox d-flex mr-12 extra_width">
                                    <input name="searchable" id="searchable_inactive" value="0" {{$store->searchable == 0?'checked':''}} class="de_active" type="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <p>{{ __('common.inactive') }}</p>
                            </li>
                        </ul>
                        <span class="text-danger" id="error_searchable"></span>
                    </div>
                </div>
                 <div class="col-xl-12">
                    <div class="primary_input">
                        <label class="primary_input_label" for="">{{ __('common.status') }}</label>
                        <ul id="theme_nav" class="permission_list sms_list ">
                            <li>
                                <label data-id="bg_option" class="primary_checkbox d-flex mr-12 extra_width">
                                    <input name="status" id="status_active" value="1" {{$store->status==1?'checked':''}} class="active"
                                        type="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <p>{{ __('common.active') }}</p>
                            </li>
                            <li>
                                <label data-id="color_option" class="primary_checkbox d-flex mr-12 extra_width">
                                    <input name="status" value="0" id="status_inactive" {{$store->status==0?'checked':''}} class="de_active" type="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <p>{{ __('common.inactive') }}</p>
                            </li>
                        </ul>
                        <span class="text-danger" id="error_status"></span>
                    </div>
                </div>
               
               <div class="col-lg-12">
                    <div class="primary_input mb-25">
                        <label class="primary_input_label" for="address">
                           {{__('common.address')}}
                            <span class="text-danger">*</span>
                        </label>
                        <input class="primary_input_field slug" type="text" id="address" name="Address" autocomplete="off" value="{{$store->Address}}" placeholder="{{__('common.address')}}">
                    </div>
                    <span class="text-danger"  id="error_address"></span>
                </div>
                 <div class="col-lg-12">
                    <div class="primary_input mb-25">
                        <label class="primary_input_label" for="pincode">
                           {{__('common.pincode')}}
                            <span class="text-danger">*</span>
                        </label>
                        <input class="primary_input_field slug" type="text" id="pincode" name="pincode" autocomplete="off" value="{{$store->pincode}}" placeholder="{{__('common.pincode')}}">
                    </div>
                    <span class="text-danger"  id="error_pincode"></span>
                </div>
                 <div class="col-lg-12">
                    <div class="primary_input mb-25">
                        <label class="primary_input_label" for="location">
                           {{__('common.location')}}
                            <span class="text-danger">*</span>
                        </label>
                        <input class="primary_input_field slug" type="text" id="location" name="location" autocomplete="off" value="{{$store->location}}" placeholder="{{__('common.location')}}">
                    </div>
                    <span class="text-danger"  id="error_location"></span>
                </div>
                 <div class="col-lg-12">
                    <div class="primary_input mb-25">
                        <label class="primary_input_label" for="mobile">
                           {{__('common.mobile')}}
                            <span class="text-danger">*</span>
                        </label>
                        <input class="primary_input_field mobile" type="text" id="mobile" name="mobile" autocomplete="off" value="{{$store->mobile}}" placeholder="{{__('common.mobile')}}">
                    </div>
                    <span class="text-danger"  id="error_address"></span>
                </div>
                 <div class="col-lg-12">
                    <div class="primary_input mb-25">
                        <label class="primary_input_label" for="email">
                           {{__('common.email')}}
                            <span class="text-danger">*</span>
                        </label>
                        <input class="primary_input_field slug" type="email" id="email" name="email" autocomplete="off" value="{{$store->email}}" placeholder="{{__('common.email')}}">
                    </div>
                    <span class="text-danger"  id="error_address"></span>
                </div>
                 <div class="col-lg-12">
                    <div class="primary_input mb-25">
                        <label class="primary_input_label" for="city">
                           {{__('common.city')}}
                            <span class="text-danger">*</span>
                        </label>
                        <input class="primary_input_field slug" type="text" id="city" name="city" autocomplete="off" value="{{$store->city}}" placeholder="{{__('common.city')}}">
                    </div>
                    <span class="text-danger"  id="error_address"></span>
                </div>
                <div class="col-lg-12">
                    <div class="primary_input mb-25">
                        <div class="primary_file_uploader" data-toggle="amazuploader" data-multiple="false" data-type="image" data-name="store_image">
                            <input class="primary-input file_amount" type="text" id="image" placeholder="{{__('common.browse_image_file')}}" readonly="">
                            <button class="" type="button">
                                <label class="primary-btn small fix-gr-bg" for="image">{{__("common.image")}} </label>
                                <input type="hidden" class="selected_files" value="{{@$store->storeImage->store_image_media->media_id}}">
                            </button>
                        </div>
                        <div class="product_image_all_div">
                            @if(@$store->storeImage->store_image_media->media_id)
                                <input type="hidden" name="store_image" class="product_images_hidden" value="{{@$store->storeImage->store_image_media->media_id}}">
                            @endif
                        </div>
                    </div>
                    @if ($errors->has('store_image'))
                        <span class="text-danger"> {{ $errors->first('store_image') }}</span>
                    @endif
                </div>
            </div>
            <div class="row mt-40">
                <div class="col-lg-12 text-center">
                    <button id="create_btn" type="submit" class="primary-btn fix-gr-bg submit_btn" data-toggle="tooltip" title=""
                        data-original-title="">
                        <span class="ti-check"></span>
                        {{__('common.update')}} </button>
                </div>
            </div>
        </div>
    </div>
</form>
